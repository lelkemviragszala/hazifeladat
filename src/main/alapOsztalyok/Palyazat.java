package main.alapOsztalyok;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Palyazat {
    

    
	
    static int PALYAZATI_OSSZEG_HATAR = 20000;

    @Override
    public String toString() {
        return (this.Sorszam+". "+this.Palyazo.toString());
    }

    public Palyazo getPalyazo() {
        return Palyazo;
    }

    public Integer getSorszam() {
        return Sorszam;
    }
    
    static int SZORGALMI_IDOSZAK_CIKLUS_MERETE = 50;
	
	private Palyazo Palyazo;
	
	private Integer Sorszam;
	
	public Palyazat(Palyazo palyazo, Integer sorszam) {
		Palyazo = palyazo;
		Sorszam = sorszam;
	}
	
	/**
	 * @return Ha az osszeghatár felett generál számot akkor -1-et ad vissza
	 */
	public int folyosithatoOsszeg(){
		
		Random rand=new Random();
		int osszeg=rand.nextInt(30000);
		
		if(osszeg>=PALYAZATI_OSSZEG_HATAR){
			return -1;
		} else {
			return osszeg;
		}
	}

	public void setPALYAZATI_OSSZEG_HATAR(int pALYAZATI_OSSZEG_HATAR) {
		PALYAZATI_OSSZEG_HATAR = pALYAZATI_OSSZEG_HATAR;
	}

    public static int getPALYAZATI_OSSZEG_HATAR() {
        return PALYAZATI_OSSZEG_HATAR;
    }

	

	public void setSZORGALMI_IDOSZAK_CIKLUS_MERETE(int sZORGALMI_IDOSZAK_CIKLUS_MERETE) {
		SZORGALMI_IDOSZAK_CIKLUS_MERETE = sZORGALMI_IDOSZAK_CIKLUS_MERETE;
	}
}
