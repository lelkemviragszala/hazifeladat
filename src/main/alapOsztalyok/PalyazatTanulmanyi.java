package main.alapOsztalyok;

public class PalyazatTanulmanyi extends Palyazat {
	
	private Integer KertOsszeg;
	private Boolean TanariAjanlas;

	public PalyazatTanulmanyi(Palyazo palyazo, Integer sorszam, Integer kertOsszeg,Boolean tanariAjanlas) {
		super(palyazo, sorszam);
		KertOsszeg=kertOsszeg;
		TanariAjanlas=tanariAjanlas;
	}
	
	
	@Override
	public int folyosithatoOsszeg() {
		
		if(TanariAjanlas){
			return KertOsszeg>PALYAZATI_OSSZEG_HATAR?PALYAZATI_OSSZEG_HATAR:KertOsszeg;
		} else {
			return -1;
		}
	}
}
