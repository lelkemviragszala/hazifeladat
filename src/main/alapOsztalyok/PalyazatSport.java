package main.alapOsztalyok;

public class PalyazatSport extends Palyazat{
	
	private Boolean EdzoiAjanlas;
	private Integer MerkozesekSzama;
	static int MERKOZESENKENTI_DIJ = 1000;

	public PalyazatSport(Palyazo palyazo, Integer sorszam, Boolean edzoiAjanlas, Integer merkozesekSzama) {
		super(palyazo, sorszam);
		// TODO Auto-generated constructor stub
		EdzoiAjanlas=edzoiAjanlas;
		MerkozesekSzama=merkozesekSzama;
                
	}
	
	@Override
	public int folyosithatoOsszeg() {
		if(EdzoiAjanlas){
			int osszeg=MERKOZESENKENTI_DIJ*MerkozesekSzama;
			return osszeg>PALYAZATI_OSSZEG_HATAR?PALYAZATI_OSSZEG_HATAR:osszeg;
		} else {
			return -1;
		}
	}

}
