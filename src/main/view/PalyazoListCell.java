/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.view;

import javafx.scene.control.ListCell;
import main.alapOsztalyok.Palyazo;

/**
 *
 * @author Administrator
 */
public class PalyazoListCell extends ListCell<Palyazo>{

    @Override
    protected void updateItem(Palyazo item, boolean empty) {
        super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
        if(item!=null){
            setText(item.toString());
        }
    }
    
    
}
