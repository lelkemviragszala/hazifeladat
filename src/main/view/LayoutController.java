package main.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import main.alapOsztalyok.*;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class LayoutController {
        private ObservableList<Palyazat> obsListNyertesPalyazatok;
	private List<Palyazat> palyazatok=new ArrayList<>();
        private List<String> palyazottak=new ArrayList<>();
	private final String FILE_PATH="/src/main/alapOsztalyok/palyazok.txt";
        
        private int sorszam;
        
        private ObservableList<Palyazo> obsListPalyazok;
        
        @FXML
        private ListView<Palyazo> listViewPalyazok;
	
	@FXML
	public void initialize(){
            listViewPalyazok.setCellFactory(lv->new PalyazoListCell());
            listaFeltoltes();
            
        }
        
        @FXML
        private Label lblTeljesOsszeg;
        
        @FXML
        private Label lblLegnagyobbKifizetes;
        
        @FXML
        private Button btnElbiralas;
        
        @FXML
        public void elbiral(ActionEvent event){
            vezerles();
            listViewNyertesPalyazatok.setCellFactory(lv->new PalyazatListCell());
            listViewNyertesPalyazatok.setItems(obsListNyertesPalyazatok);
            int osszeg=0;
            for(Palyazat nyertesPalyazat:obsListNyertesPalyazatok){
                osszeg+=nyertesPalyazat.folyosithatoOsszeg();
            }
            lblTeljesOsszeg.setText("Teljes összeg: "+osszeg);
            int maxFolyositas=0;
            for(Palyazat nyertesPalyazat:obsListNyertesPalyazatok){
                if(nyertesPalyazat.folyosithatoOsszeg()>maxFolyositas){
                    maxFolyositas=nyertesPalyazat.folyosithatoOsszeg();
                }
            }
            lblLegnagyobbKifizetes.setText("A legnagyobb kifizetés "+maxFolyositas);
        }
        /**
         * @return A szorgalmiIdoszakot 50-szer meghívja, feltökti a nyertes pályázatok listáját
         */
        public void vezerles(){
            sorszam=0;
            for(int i=0;i<50;i++){
                szorgalmiIdoszak();
            }
            obsListNyertesPalyazatok=FXCollections.observableArrayList();
            
            for(Palyazat palyazat:palyazatok){
                if(palyazat.folyosithatoOsszeg()>-1){
                    obsListNyertesPalyazatok.add(palyazat);
                }
            }
        }
        
        @FXML
        private ListView<Palyazat> listViewNyertesPalyazatok;
        
        /**
         * @return A txt alapján feltöltött listábból kiválaszt 2 Palyazo-t majd meghívja a tanul és sportol metódusait, aztán megnézi az EdzoiAjanlas-t és a TanariAjanlast
         */
        public void szorgalmiIdoszak(){
            Random rand=new Random();
            Palyazo elsopalyazo=obsListPalyazok.get(rand.nextInt(obsListPalyazok.size()));
            Palyazo masodikPalyazo=obsListPalyazok.get(rand.nextInt(obsListPalyazok.size()));
            elsopalyazo.sportol();
            
            masodikPalyazo.tanul();
           
            if(elsopalyazo.isEdzoiAjanlas()&&!palyazottak.contains(elsopalyazo.toString())){
                sorszam++;
                palyazottak.add(elsopalyazo.toString());
                PalyazatSport sportPalyazat=new PalyazatSport(elsopalyazo, sorszam, true, elsopalyazo.getMerkozesekSzama());
                palyazatok.add(sportPalyazat);
                for(Palyazat tartalmazza:palyazatok){
                    System.out.println(tartalmazza);
                }
            }
            if(masodikPalyazo.isTanariAjanlas()&&!palyazottak.contains(masodikPalyazo.toString())){
                sorszam++;
                palyazottak.add(masodikPalyazo.toString());
                PalyazatTanulmanyi tanulmanyiPalyazat=new PalyazatTanulmanyi(masodikPalyazo, sorszam, rand.nextInt(2*Palyazat.getPALYAZATI_OSSZEG_HATAR()), true);
                palyazatok.add(tanulmanyiPalyazat);
                for(Palyazat tartalmazza:palyazatok){
                    System.out.println(tartalmazza);
                }
                
            } 
        }
        /**
         * @return A txt alapján feltölti a Palyazo-t tartalmazo ObservableList-t.
         */
        private void listaFeltoltes(){
            //Harmat Péter;HAPEABC.PTE
            String filePath = new File("").getAbsolutePath();
            System.out.println (filePath);

            try(BufferedReader bemenet = new BufferedReader(new FileReader(filePath+FILE_PATH))){
                                  
            obsListPalyazok=FXCollections.observableArrayList();
            String sor;
                while((sor = bemenet.readLine()) != null)
                {
                    sor=sor.trim();
                    String[] palyazoAdatai=sor.split(";");
                    Palyazo hallgato=new Palyazo(palyazoAdatai[0], palyazoAdatai[1]);
                    obsListPalyazok.add(hallgato);
                }
                Collections.sort(obsListPalyazok, new PalyazoNevSzerint());
                listViewPalyazok.setItems(obsListPalyazok);
            } catch(IOException e){
                e.printStackTrace();
                Alert messageBox=new Alert(AlertType.ERROR);
	        messageBox.setTitle("Hiba a fájl beolvasása közben!");
		messageBox.setContentText(":(");
		messageBox.showAndWait();
            }
        }

}
