/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import main.alapOsztalyok.Palyazat;
import main.alapOsztalyok.PalyazatSport;
import main.alapOsztalyok.PalyazatTanulmanyi;
import main.alapOsztalyok.Palyazo;
import main.view.PalyazoNevSzerint;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class Teszt {
    
    public Teszt() {
    }
    
    @BeforeClass
    public static void setUpClass() {
      
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testSzorgalmiidoszak() {
         
            ObservableList<Palyazat> obsListNyertesPalyazatok;
            List<Palyazat> palyazatok=new ArrayList<>();
            List<String> palyazottak=new ArrayList<>();
            final String FILE_PATH="/src/main/alapOsztalyok/palyazok.txt";
            
            ObservableList<Palyazo> obsListPalyazok=FXCollections.observableArrayList();
            
            String filePath = new File("").getAbsolutePath();
            System.out.println (filePath);

            try(BufferedReader bemenet = new BufferedReader(new FileReader(filePath+FILE_PATH))){
                                  
            obsListPalyazok=FXCollections.observableArrayList();
            String sor;
                while((sor = bemenet.readLine()) != null)
                {
                    sor=sor.trim();
                    String[] palyazoAdatai=sor.split(";");
                    Palyazo hallgato=new Palyazo(palyazoAdatai[0], palyazoAdatai[1]);
                    obsListPalyazok.add(hallgato);
                }
                
            } catch(IOException e){
                e.printStackTrace();
                Alert messageBox=new Alert(Alert.AlertType.ERROR);
	        messageBox.setTitle("Hiba a fájl beolvasása közben!");
		messageBox.setContentText(":(");
		messageBox.showAndWait();
            }
            
            
            int sorszam=0;
      
            Random rand=new Random();
            Palyazo elsopalyazo=obsListPalyazok.get(rand.nextInt(obsListPalyazok.size()));
            Palyazo masodikPalyazo=obsListPalyazok.get(rand.nextInt(obsListPalyazok.size()));
            elsopalyazo.sportol();
            System.out.println(elsopalyazo.toString()+"sportolt.");
            masodikPalyazo.tanul();
            System.out.println(masodikPalyazo.toString()+"tanult.");
            if(elsopalyazo.isEdzoiAjanlas()&&!palyazottak.contains(elsopalyazo.toString())){
                sorszam++;
                palyazottak.add(elsopalyazo.toString());
                PalyazatSport sportPalyazat=new PalyazatSport(elsopalyazo, sorszam, true, elsopalyazo.getMerkozesekSzama());
                palyazatok.add(sportPalyazat);
                for(Palyazat tartalmazza:palyazatok){
                    System.out.println(tartalmazza);
                }
            }
            if(masodikPalyazo.isTanariAjanlas()&&!palyazottak.contains(masodikPalyazo.toString())){
                sorszam++;
                palyazottak.add(masodikPalyazo.toString());
                PalyazatTanulmanyi tanulmanyiPalyazat=new PalyazatTanulmanyi(masodikPalyazo, sorszam, rand.nextInt(2*Palyazat.getPALYAZATI_OSSZEG_HATAR()), true);
                palyazatok.add(tanulmanyiPalyazat);
                for(Palyazat tartalmazza:palyazatok){
                    System.out.println(tartalmazza);
                }
                
            }
            if(palyazottak.contains(elsopalyazo.toString())||palyazottak.contains(masodikPalyazo.toString())){
                if(palyazottak.contains(elsopalyazo.toString())){
                    System.out.println("Tartalmazza őt:"+elsopalyazo.toString());
                } else if(palyazottak.contains(masodikPalyazo.toString())){
                    System.out.println("Tartalmazza őt:"+masodikPalyazo.toString());
                }
            }
        }
}
